export const HIVONISMS = {
  "fullSayings": [
    "затни се",
    "wow чоек олабави малку",
    "уќути се глупаку",
    "Каков предосаден глупак господе земи ме",
    "Убиј се",
    "... sad",
    "Оф.. тажно суштество",
    "ако те игнорирам дали ќе прекинеш?",
    "Гррррррррр",
    "хахах одвратно",
    "Imam dobro srce",
    "Ološu zaveži usta",
    "Леле...",
    "хахаха глупо едно",
    "врло смешно, чекај да се насмеам со капс \"XAXAXA\"",
    "тебе на улица да те видам легнат ќе те прескокнам",
    "Мрш",
    "U boring piece of shit",
    "ај ебете се глупи чао ",
    "не",
    "Неочекувано од тебе ама фајн",
    "Имам добро срце",
    "греота",
    "калаштурo!",
    "одвратна екипица",
    "Шо без мене бе?",
    "ќе ти го скршам вратчето",
    "ќе се воздржам до коментари..",
    "Фацата ти е промашај",
    "Иу",
    "Леле... УЖАС!",
    ":)",
    "не ме замарај",
    "трагично е колку си опседнат со мене а јас стварно не сакам да те дружам.. сори :(",
    "a, не прочитав сори",
    "Ццц!!!",
  ],
  "partialComebacks": [
    {
      "comeback": "Фацата ти е ",
      "placeAt": "startOfSentence"
    },
  ],
  "personalizedSayings": [
    {
      "comeback": "скини ми се с курца педер",
      "placeNameAt": "startOfSentence",
    },
    {
      "comeback":  "Светски ден на заќути",
      "placeNameAt": "endOfSentence",
    },
    {
      "comeback": "греота си",
      "placeNameAt": "startOfSentence",
    },
    {
      "comeback": "кретену еден глупав",
      "placeNameAt": "startOfSentence",
    },
  ]
};

export const partialComebacksTriggerWords = ["глуп", "глупа", "глупо", "грд", "грда", "грдо", "смешен", "смешна", "смешно", "смотан", "смотана", "смотано"];

export const CHAT_STACK_LOCALSTORAGE_NAME = 'hgpt-chatstack';
