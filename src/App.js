import './App.css';

import ChatMessageInput from './components/chat-message-input/chat-message-input.js';
import ChatWindow from './components/chat-window/chat-window.js';

import { useState, useEffect, useRef } from 'react';

import { ChatEngine } from './utils/chat-engine.js'

function App() {
  const bot = new ChatEngine();

  const [chatState, setChatState] = useState(bot.getStoredChatStack());
  const [chatResponded, setChatResponded] = useState(true);
  const [generatingResponse, setGeneratingResponse] = useState(false);

  const chatWindowElRef = useRef(null);

  function generateResponse() {
    if (chatResponded) return;
    const newState = chatState.slice();
    const lastMessage = chatState[chatState.length - 1].message;
    newState.push({
      from: 'chat',
      message: bot.processInput(lastMessage),
      id: chatState.length + 1,
    });
    setChatState(newState);
    bot.setStoredChatStack(newState);
    setChatResponded(true);
    setGeneratingResponse(false);
  }

  function onMessageSent(message) {
    if (!message.length) return;
    const newState = chatState.slice();
    newState.push({
      from: 'user',
      message: message,
      id: chatState.length + 1,
    });
    setChatState(newState);
    bot.setStoredChatStack(newState);
    setChatResponded(false);
    setGeneratingResponse(true);
  }

  useEffect(() => {
    chatWindowElRef?.current?.lastElementChild?.scrollIntoView({ behavior: 'smooth' });
    setTimeout(generateResponse, 1000);
  })

  return (
    <div>
      <ChatWindow chatState={chatState} elementRef={chatWindowElRef}/>
      <ChatMessageInput onMessageSent={message => onMessageSent(message)} disabled={generatingResponse}/>
    </div>
  );
}

export default App;
