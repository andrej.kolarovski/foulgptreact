import { HIVONISMS, partialComebacksTriggerWords, CHAT_STACK_LOCALSTORAGE_NAME } from '../constants/constants.js';

export class ChatEngine {
  genericReplyStack = [];
  reuseWordReplyStack = [];
  personalizedReplyStack = [];
  username;

  deepCopy(object) {
    return JSON.parse(JSON.stringify(object));
  }

  stir(array) {
    let currentIndex = array.length,  randomIndex;

    // While there remain elements to shuffle.
    while (currentIndex != 0) {

      // Pick a remaining element.
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;

      // And swap it with the current element.
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }

    return array;
  }

  getStoredChatStack() {
    return JSON.parse(localStorage.getItem(CHAT_STACK_LOCALSTORAGE_NAME)) || [];
  }

  setStoredChatStack(chatStack) {
    localStorage.setItem(CHAT_STACK_LOCALSTORAGE_NAME, JSON.stringify(chatStack));
  }

  initStacks() {
    this.genericReplyStack = this.deepCopy(HIVONISMS.fullSayings);
    this.reuseWordReplyStack = this.deepCopy(HIVONISMS.partialComebacks);
    this.personalizedReplyStack = this.deepCopy(HIVONISMS.personalizedSayings);
  }

  mapPartialComeback(triggerWord) {
    const index = partialComebacksTriggerWords.indexOf(triggerWord)
    let responseWord;

    if ((index + 1) % 3 === 0) {
      responseWord = partialComebacksTriggerWords[index - 1];
    }

    if (index % 3 === 0) {
      responseWord = partialComebacksTriggerWords[index + 1];
    }

    if ((index - 1) % 3 === 0) {
      responseWord = partialComebacksTriggerWords[index];
    }

    if (!this.reuseWordReplyStack.length)
      this.reuseWordReplyStack = this.deepCopy(HIVONISMS.partialComebacks);

    this.reuseWordReplyStack = this.stir(this.reuseWordReplyStack);
    const response = this.reuseWordReplyStack.pop();
    let ret;

    switch (response.placeAt) {
      case 'startOfSentence': ret = `${response.comeback} ${responseWord}`; break;
      case 'endOfSentence': ret = `${responseWord} ${response.comeback}`; break;
      default: break;
    }

    return ret;
  }

  mapPersonalizedComeback() {
    if (!this.personalizedReplyStack.length)
      this.personalizedReplyStack = this.deepCopy(HIVONISMS.personalizedSayings);

    this.personalizedReplyStack = this.stir(this.personalizedReplyStack);
    const replyData = this.personalizedReplyStack.pop();

    let username$ = this.username || 'Андреј';

    let ret;
    if (replyData.placeNameAt === 'startOfSentence')
      return `${username$} ${replyData.comeback}`;
    else return `${replyData.comeback} ${username$}`;
  }

  mapComeback() {
    if (!this.genericReplyStack.length)
      this.genericReplyStack = this.deepCopy(HIVONISMS.fullSayings);

    this.genericReplyStack = this.stir(this.genericReplyStack);
    const ret = this.genericReplyStack.pop();

    return ret;
  }

  processInput(userSentence) {
    if (
      userSentence.toLowerCase().includes('зајче') ||
      userSentence.toLowerCase().includes('zajce') ||
      userSentence.toLowerCase().includes('zajche')
    )
      return 'Иу'

    for (let i = 0; i < partialComebacksTriggerWords.length; i++) {
      const triggerWord = partialComebacksTriggerWords[i];
      if (userSentence.toLowerCase().includes(triggerWord))
        return this.mapPartialComeback(triggerWord);
    }
    return Math.random() > .15 ? this.mapComeback() : this.mapPersonalizedComeback();
  }

  constructor() {
    this.initStacks();
  }
}
