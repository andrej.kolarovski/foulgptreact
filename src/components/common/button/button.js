import './button.css';

export default function Button({ buttonText, clickHandler, className, type, disabled}) {
  return (
    <button
      className={`Button ${className}`}
      onClick={clickHandler}
      type={type || 'button'}
      disabled={disabled}
    >
      {buttonText || 'I\'m a Button'}
    </button>
  );
}
