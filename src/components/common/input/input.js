import './input.css';

export default function Input({ type, value, onChange, className, disabled }) {
  return (
    <input
      className={`App-input ${className}`}
      type={type || 'text'}
      value={value}
      onChange={e => onChange(e.target.value)}
      disabled={disabled}
    />
  );
}
