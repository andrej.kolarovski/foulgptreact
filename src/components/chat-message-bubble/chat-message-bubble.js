import './chat-message-bubble.css';

export default function ChatBubble({ chatMessage }) {
  return (
    <div className={`ChatMessage ${chatMessage.from == 'user' ? "UserMessage" : "ResponseMessage"}`}>
      {chatMessage.message}
    </div>
  );
}
