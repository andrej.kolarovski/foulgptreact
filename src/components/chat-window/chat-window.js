import './chat-window.css';

import ChatMessageBubble from '../chat-message-bubble/chat-message-bubble.js';

export default function ChatWindow({ chatState, elementRef }) {
  return (
    <div className="ChatWindow" ref={elementRef}>
      {chatState.map(messageState => <ChatMessageBubble key={messageState.id} chatMessage={messageState}/>)}
    </div>
  );
}
