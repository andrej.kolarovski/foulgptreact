import './chat-message-input.css';

import Input from '../common/input/input.js';
import Button from '../common/button/button.js';

import { useState } from 'react';

export default function ChatMessageInput({ type, className, onMessageSent, disabled }) {
  const [messageInputState, setMessageInputState] = useState('');

  function sendMessage(event) {
    event.preventDefault();
    onMessageSent(messageInputState);
    setMessageInputState('');
  }
  return (
    <form onSubmit={e => sendMessage(e)}>
      <Input className="Main-Input" value={messageInputState} onChange={value => setMessageInputState(value)} disabled={disabled}/>
      <Button className="Main-Button" buttonText="Send" type="submit" disabled={disabled}/>
    </form>
  );
}
